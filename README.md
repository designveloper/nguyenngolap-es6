# ECMAScript 6
Intern: Nguyen Ngo Lap

## 1. What is ES6?

- 1995 - JS created
- 1997 - ES 1
- 2009 - ES 5: New array method (each, map, filter)
- 2015 - ES 6: Official spec
    - New keywords: let, const
    - Functions: default parameters and arrow functions
    - Classes

## 2. Transpiling ES6

\- Babel:

- ES6 code in -> ES5 code out
- Created by Sebastian McKenzie
- Used frequently with React

\- Use **webpack** to automate the transpiling process

\- The babel package is no more. Previously, it was the entire compiler and all the transforms plus a bunch of CLI tools, but this lead to unnecessarily large downloads and was a bit confusing. Now it's split up into two separate packages: `babel-cli` and `babel-core`.

## 3. ES6 syntax

\- `let`

\- `const`

\- Template strings:

- Format JS code (string) with variables
- Can make longer, better strings with variables and custom formatting (dynamic content)

\- `...`: An array within an array

## 4. ES6 functions and objects
\- Arrow functions:

- `=>`: Make the code more compact and readable, and help deal with the scope of `this`
- Reference: https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/

\- Generators:

- Allow us to pause functions
- Pause with `yield`
- Get next yield with `.next()`
- Sample code:
```
function* director() {
    yield "Three";
    yield "Two";
    yield "One";
    yield "Action!";
}

var action = director();

console.log(action.next());
console.log(action.next());
console.log(action.next());
console.log(action.next());
```

## 5. ES6 classes
\- React Classes sample code:
```
class Restaurant extends React.Component {
    render() {
        return (<div>
                <h1>{this.props.name}</h1>
            </div>)
    }
}

React.render(<Restaurant name="Matey's Shrimp Shack"></Restaurant>, document.body);
```
